#                     __ _ _
#    _ __  _ __ ___  / _(_) | ___
#   | '_ \| '__/ _ \| |_| | |/ _ \
#  _| |_) | | | (_) |  _| | |  __/
# (_) .__/|_|  \___/|_| |_|_|\___|
#   |_|
#

# start grphical environment on tty1 if i3 not running
#[ "$(tty)" = "/dev/tty1" ] && ! pgrep -x i3 >/dev/null && exec xinit

# default programs
export TERMINAL="st"
export EDITOR="vim"
export BROWSER="firefox"
export MAIL="thunderbird"
export PLAYER="vlc"
export SHELL="/usr/bin/zsh"

# system variables
export AUR="https://aur.archlinux.org"

# switch caps_lock and escape and y and z on tty
sudo -n loadkeys ~/.ttymaps.kmap 2>/dev/null
