#!/bin/sh

DOTFILES=".zshrc .aliases .profile .tmux.conf .config/nvim"

# get current location
DOTFILE_DIR=$(realpath --relative-to=$HOME $(dirname $(readlink -f $0)))
BACKUP_DOTFILE_DIR=$HOME/.dotfiles.bak

# create location to backup old dotfiles
mkdir $BACKUP_DOTFILE_DIR

# backup old dotfiles and replace with symlinks to new ones
for f in $DOTFILES
do
	mv $HOME/$f $BACKUP_DOTFILE_DIR
	ln -sf $(realpath $f) $HOME/$f
done

