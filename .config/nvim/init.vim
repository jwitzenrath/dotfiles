"
" ~/.vimrc
"

" manage plugins
call plug#begin('~/.vim/plugged')
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-airline/vim-airline'
Plug 'morhetz/gruvbox'
Plug 'scrooloose/nerdtree'
Plug 'ryanoasis/vim-devicons'
"Plug 'dylanaraps/wal.vim'
Plug 'tpope/vim-surround'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'alvan/vim-closetag'
call plug#end()

" general setings
syntax on
set nocompatible
set nohlsearch
set encoding=UTF-8
set incsearch
set number relativenumber
set clipboard=unnamedplus
set wildmode=longest,list,full
set splitbelow splitright
"set listchars=eol:¬
"set list

" status bar
let g:airline_theme='base16_chalk'
set background=dark
"colorscheme gruvbox

" easy pane switching
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" run python on <F5>
map <F5> :!python3 %<CR>

" NERDTree
map <C-n> :NERDTreeToggle<CR>

" set relativenumbers in normal mode and absolut in insert mode
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" moving lines in normal, insert and visual mode
nnoremap <C-j> :m+1<CR>==
nnoremap <C-k> :m-2<CR>==
inoremap <C-j> <ESC>:m .+1<CR>==gi
inoremap <C-k> <ESC>:m .-2<CR>==gi
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

" stuff done when saving files
autocmd BufWritePost $HOME/.vimrc source %
"autocmd BufWritePost $HOME/.bash* !source %
autocmd BufWritePost $HOME/.config/i3/config !i3 reload
autocmd BufWritePost $HOME/.xmodmap !xmodmap %
autocmd BufWritePost *Xresources,*Xdefualt !xrdb %

" remove trailing whitespaces on save
autocmd BufWritePre * %s/\s\+$//e

" stuff done when closing vim
